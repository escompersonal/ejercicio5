/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author carlos
 */
public class Hecho implements Serializable{
    
    private int id;
    private String nombre;

    /**
     * 
     * @return id obtenido de la BD
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id: establece un id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * 
     * @return nombre obtenido de la BD
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre : Nombre con el cual se guardará
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public String toString() {
        return "Hecho{" + "id=" + id + ", nombre=" + nombre + '}';
    }

    


    
    
}
