/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;
import java.sql.SQLException;

/**
 *
 * @author carlos
 */
public interface CRUD {
    
    /**
     * spINSERT(in _nombre VARCHAR(50))
     */
    public static final String SQL_INSERT
            = "{ call spINSERT ("
            + " ?)}";
    /**
     * spREAD(IN _idHecho INT)
     */
    public static final String SQL_SELECT
            = "{ call spREAD("
            + "?) }";
    /**
     * spREAD_ALL()
     */
    public static final String SQL_SELECT_ALL
            = "{call spREAD_ALL()}";
    
    public static final String SQL_UPDATE
            = "{call spUPDATE("
            + "? , ?)}";
    /**
     *  spDELETE(in _idHecho INT)
     */
    public static final String SQL_DELETE
            = "{call spDELETE("
            + "?)}";
    /**
     * @param h : Hecho que va a ser agregado a la base de datos
     * @return : True si se realizo la insercion 
     * @throws SQLException 
     */
    public boolean alta(Hecho h) throws SQLException;

    public boolean baja(Hecho h) throws SQLException;

    public boolean editar(Hecho h) throws SQLException;

    public Hecho buscar(Hecho id) throws SQLException;

    public List<Hecho> listar() throws SQLException;
}
