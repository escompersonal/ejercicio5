/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class HechoDAO {

    /**
     * 
     * @param dto : Contiene los datos que seran insertados
     * @param conn : Reliza la conexion a l a BD
     * @return : True si la insercion fue correcta
     * @throws SQLException 
     */
    public boolean create(Hecho dto, Connection conn) throws SQLException {
        CallableStatement cs = null;
        try {
            cs = conn.prepareCall(CRUD.SQL_INSERT);            
            cs.setString(1, dto.getNombre());
            return !(0 == cs.executeUpdate());
        } finally {
            cerrar(cs);
            cerrar(conn);
        }
    }
    
    /**
     * 
     * @param dto : Contiene los datos que seran insertados
     * @param conn : Reliza la conexion a l a BD
     * @return : True si se elimino correctamente
     * @throws SQLException 
     */
    public boolean delete(Hecho dto, Connection conn) throws SQLException {
        CallableStatement cs = null;
        try {
            cs = conn.prepareCall(CRUD.SQL_DELETE);
            cs.setInt(1, dto.getId());
            return !(0 == cs.executeUpdate());
        } finally {
            cerrar(cs);
            cerrar(conn);
        }
    }
    /**
     * 
     * @param dto : Contiene los datos que seran insertados
     * @param conn : Reliza la conexion a l a BD
     * @return : True si se actualizó correctamente
     * @throws SQLException 
     */
    public boolean update(Hecho dto, Connection conn) throws SQLException {
        CallableStatement cs = null;
        try {
            cs = conn.prepareCall(CRUD.SQL_UPDATE);
            cs.setInt(1, dto.getId());
            cs.setString(2, dto.getNombre());            
            return !(0 == cs.executeUpdate());
        } finally {
            cerrar(cs);
            cerrar(conn);
        }
    }
    /**
     * 
     * @param dto : Contiene los datos que seran insertados
     * @param conn : Reliza la conexion a l a BD
     * @return : Hecho leido  de la BD
     * @see Hecho
     * @throws SQLException  
     */
    public Hecho load(Hecho dto, Connection conn) throws SQLException {
        CallableStatement cs = null;
        ResultSet rs = null;
        try {
            cs = conn.prepareCall(CRUD.SQL_SELECT);
            cs.setInt(1, dto.getId());
            rs = cs.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (Hecho) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(cs);
            cerrar(conn);
        }
    }
    /**
     * 
     * @param conn
     * @return Lista de hechos leidos de la BD
     * @see Hecho
     * @throws SQLException 
     */
    public List loadAll(Connection conn) throws SQLException {
        CallableStatement cs = null;
        ResultSet rs = null;
        try {
            cs = conn.prepareCall(CRUD.SQL_SELECT_ALL);
            rs = cs.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(cs);
            cerrar(conn);
        }
    }
    
    /**
     * Opera el ResulSet generado por otra metodo
     * @param rs : ResultSet que contiene la informacion de la operacion realizada en la BD
     * @return : Lista de Hechos obtenidos de la BD
     * @see ResultSet
     * @throws SQLException 
     */
    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            Hecho dto = new Hecho();
            dto.setId(rs.getInt("idHecho"));
            dto.setNombre(rs.getString("nombreHecho"));
            results.add(dto);
        }
        return results;
    }
    /**
     * Cierra el flujo  del CallableStatement
     * @param cs : CallableStatement que será cerrado
     * @throws SQLException 
     * @see  CallableStatement
     */
    private void cerrar(CallableStatement cs) throws SQLException {
        if (cs != null) {
            try {
                cs.close();
            } catch (SQLException e) {
            }
        }
    }
    /**
     * Cierra el flujo de un ResultSet
     * @param rs : ResultSet que será cerrado
     * @see  ResultSet
     */
    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }
    /**
     * Cierra el flujo de una conexion a la BD
     * @param cnn : Connection que será cerrada
     * @see Connection
     */
    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }

}
