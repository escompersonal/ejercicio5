/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hechos;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import modelo.Hecho;

/**
 *
 * @author carlos
 */
public class ClienteBD {

    private Socket socket;

    /*public Object sendRequest(int request) throws IOException, ClassNotFoundException {
        socket = new Socket("localhost", ServidorBD.PORT);

        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

        output.writeInt(request);
        output.flush();
        Object list = (List) input.readObject();
        input.close();
        output.close();
        return list;
    }*/

    public Object sendRequest(int request, Hecho he) throws IOException, ClassNotFoundException {
        socket = new Socket("localhost", ServidorBD.PORT);
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        output.writeInt(request);
        output.writeObject(he);
        output.flush();

        Object o = input.readObject();
        input.close();
        output.close();
        return o;
    }

    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
    }

}
