/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hechos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import modelo.CRUD;
import modelo.Hecho;
import modelo.HechoDAO;


/**
 *
 * @author carlos
 * @see CRUD
 */
public class ManagerConexion implements CRUD {

    private Connection cnn;
    private HechoDAO dao;

    public ManagerConexion() {
        String user = "root";
        String pwd = "root";
        String url = "jdbc:mysql://localhost:3306/Distribuidos";
        String mySqlDriver = "com.mysql.jdbc.Driver";
        dao = new HechoDAO();
        try {
            Class.forName(mySqlDriver);
            cnn = DriverManager.getConnection(url, user, pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
 
    @Override
    public boolean alta(Hecho h) throws SQLException {
        return dao.create(h, cnn);
    }

    @Override
    public boolean baja(Hecho h) throws SQLException {
        return dao.delete(h, cnn);
    }

    @Override
    public boolean editar(Hecho h) throws SQLException {
        return dao.update(h, cnn);
    }

    @Override
    public Hecho buscar(Hecho h) throws SQLException {
        return dao.load(h, cnn);
    }

    @Override
    public List<Hecho> listar() throws SQLException {
        return dao.loadAll(cnn);
    }

   

}
